/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1906526
 */
public class Circulo extends Elipse{
    double  raio;

    public Circulo(double raio){
        super(raio, raio);
        this.raio = raio;
    }
    
    
    @Override
    public double getPerimetro(){
        double p = 2*Math.PI*raio;
        return p;
    }
    
    @Override
    public String getNome() {
        return "Circulo";
    }
}
