/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1906526
 */
public class Elipse implements FiguraComEixos{
    public double semieixoMaior, semieixoMenor;
    
    public Elipse(){
        
    }
    
    public Elipse(double semieixoMaior, double semieixoMenor){
        this.semieixoMaior = semieixoMaior;
        this.semieixoMenor = semieixoMenor;
    }
    
    @Override
    public double getArea(){
        double a = Math.PI*semieixoMaior*semieixoMenor;
        return a;
    }
    
    @Override
    public double getPerimetro(){
        double p = Math.PI * (3 * (semieixoMaior + semieixoMenor) - Math.sqrt((3*semieixoMaior + semieixoMenor)*(semieixoMaior + 3*semieixoMenor)));
        return p;
    }
    
    @Override
    public double getEixoMenor(){
        double eixoMenor = 2*semieixoMenor;
        return eixoMenor;
    }
    
    @Override
    public double getEixoMaior(){
        double eixoMaior = 2*semieixoMaior;
        return eixoMaior;
    }

    @Override
    public String getNome() {
        return "Elipse";
    }
    
}
