
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a1906526
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse a = new Elipse(8, 6);
        Circulo b = new Circulo(5);
        double area, perimetro;
        String nome;
        
        area = a.getArea();
        perimetro = a.getPerimetro();
        nome = a.getNome();
        
        System.out.println("Nome da figura: " + nome);
        System.out.println("Area da elipse: " + area);
        System.out.println("Perimetro da elipse: " + perimetro);
        
        area = b.getArea();
        perimetro = b.getPerimetro();
        nome = b.getNome();
        
        System.out.println("Nome da figura: " + nome);
        System.out.println("Area do circulo: " + area);
        System.out.println("Perimetro do circulo: " + perimetro);
    }
}
